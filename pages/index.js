
import Link from 'next/link'
import Router from 'next/router'

import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { fetchListProduct, saveDetailPage } from '../store/actions/homeAction'

import MetaData from '../components/MetaData.js'
import ProductCard from '../components/productCard'

export default function Home() {

  let listArray = null;
  const [sortVal, setSortVal] = React.useState('0');

  let Logo = `https://s.catch.com.au/static/catch/images/logo-8b0ef96c7b.svg`;
  
  const dispatch = useDispatch();
  const { listProduct } = useSelector( state => state.listProduct );

  
  if(listProduct) {
      listArray = listProduct
  }

  useEffect (() => {
      dispatch(fetchListProduct())
      // console.log('process.env.ENV_VARIABLE : ', process.env.ENV_VARIABLE)
      console.log('process.env.NEXT_PUBLIC_ENV_VARIABLE ', process.env.NEXT_PUBLIC_ENV_VARIABLE)
      console.log('process.env.NEXT_PUBLIC_DEVELOPMENT_ENV_VARIABLE ', process.env.NEXT_PUBLIC_DEVELOPMENT_ENV_VARIABLE)
      console.log('process.env.NEXT_PUBLIC_PRODUCTION_ENV_VARIABLE ', process.env.NEXT_PUBLIC_PRODUCTION_ENV_VARIABLE)
  }, [])

  function saveData (val) {
      dispatch(saveDetailPage(val))
  }

  function handleSort (event) {
    const newVal = event.target.value
    setSortVal(newVal)
    console.log(newVal)
    if (newVal == '1') {
      listArray.sort(function (a, b) {
        return b.salePrice - a.salePrice;
      });
    } else if (newVal == '2') {
      listArray.sort(function (a, b) {
        return a.salePrice - b.salePrice;
      });
    } else {}
  }

  return (
    <div className="container">
      <MetaData 
          titlePage={`Catch.com.au Frontend Test`}
          description={`Low prices on the biggest brands in fashion, tech, beauty, grocery, sports, and more at Catch. Huge savings every day! It's massive! Formerly Catchoftheday - now Catch!`}
          title={`Product Page`}
          keyword={`Product Page`}
          image={Logo}
          url="https://fe-test-catch.indragawe73.vercel.app/"
      />

      <main>
        <div className="container">

          <div className="row wrap_header justify-content-between">
            <div className="col-4 handle_col_5">
              <img className="img_logo" src={Logo} alt="main logo"/>
            </div>
            <div className="col-3 handle_col_5 justify-content-end ">
              <div className="wrap_sorting_array">
                <label className="label_sort">Sort by:</label>
                <select className="sorting_array cursor_link" onChange={handleSort} value={sortVal}>
                    <option key='0' value='0'>SUGGESTION</option>
                    <option key='1' value='1'>HIGHEST PRICE</option>
                    <option key='2' value='2'>LOWEST PRICE</option>
                </select>
              </div>
            </div>
          </div>
          <div className="row justify-content-start">

            {listArray ?
              listArray.map((item, index) => {
                return (

                  <Link key={index} href={{ pathname: '/detailPage', query: { data: JSON.stringify(item) } }} prefetch>
                    <div onClick={() => saveData(item)} className="col-3 handle_col_5 cursor_link">
                      <ProductCard listArray={item} /> 
                    </div>
                  </Link>                 
                )
              })
            :null}
          </div>
        </div>
      </main>
    </div>
  )
}
