import Link from 'next/link'
import MetaData from '../components/MetaData.js'

import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'

import Router, { useRouter } from 'next/router'

import ProductCard from '../components/productCard'

const DetailPage = ( props ) => {

    let detail = null;
    const router = useRouter()
    
    let Logo = `https://s.catch.com.au/static/catch/images/logo-8b0ef96c7b.svg`;
    detail = JSON.parse(router.query.data);

    // detail = useSelector( state => state.detailPage.detailPage );
    // console.log('detailPage', detail)

    function goDollar (val) {
        const goString = val.toString();
        const goLength = goString.length;
        const price = goLength - 2;
        const result = goString.substring(0, price);
        return result
    }

    function goCent (val) {
        const goString = val.toString();
        const goLength = goString.length;
        const price = goLength - 2;
        const result = goString.substring(price, goLength);
        return result
    }

    return (
    <div className="container">
        <MetaData 
            titlePage={`Detail Page | ${detail.name}`}
            description={`Detail Page | ${detail.name}`}
            title={`Detail Page | ${detail.name}`}
            keyword={detail.name}
            image={detail.imageUrl}
            url="https://fe-test-catch.indragawe73.vercel.app/"
        />
        <div className="row wrap_header justify-content-between">
            <div className="col-4 handle_col_5">
                <Link href={{ pathname: '/' }}>
                    <img className="img_logo cursor_link" src={Logo} alt="main logo" />
                </Link>
            </div>
        </div>

        {detail ?

            <div className="row justify-content-start">
                <div className="col-12 handle_col_5 cursor_link">
                    <ProductCard listArray={detail} />  
                </div>
            </div>
        :null}
    </div>
    )
};  

DetailPage.getInitialProps = async (context) => {
    let pathId = null;
    

    return {
        pathId: pathId
    }
};

export default DetailPage;
