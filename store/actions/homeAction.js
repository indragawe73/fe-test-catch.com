import * as types from '../types'

export const fetchListProduct = () => async dispatch => {
	const proxyurl = "https://cors-anywhere.herokuapp.com/";
	const url = "http://catch-code-challenge.s3-website-ap-southeast-2.amazonaws.com/challenge-3/response.json";
	
	fetch(proxyurl + url) 
	.then(response => response.json())
	.then(contents => {
		
		dispatch({
			type: types.GET_LISTPRODUCT,
			payload: contents.results,
		})		
	
	})
	.catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"))
};

export const saveDetailPage = (val) => async dispatch => {
		
	dispatch({
		type: types.GET_DETAILPAGE,
		payload: val,
	})		
	
};
