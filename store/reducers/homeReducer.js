import * as types from '../types'

const initialState={
	listProduct: null,
	detailPage: null,
}

export default function(state = initialState, action) {
	switch (action.type) {

		case types.GET_LISTPRODUCT:
			return {
				...state,
				listProduct: action.payload,
			}

		case types.GET_DETAILPAGE:
			console.log('zzzzz', action.payload)
			return {
				...state,
				detailPage: action.payload,
			}


		default: 
			return state
	}
}