import { combineReducers } from 'redux'

import listProduct from './homeReducer'
import detailPage from './homeReducer'

export default combineReducers({
	listProduct: listProduct,
	detailPage: detailPage,
})