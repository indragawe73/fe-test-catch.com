This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Please Follow the steps :

1. Clone the repository https://bitbucket.org/indragawe73/fe-test-catch.com/src/master/

2. Run `npm install` or `yarn istall` in the root folder from your CLI

3. To running the development server `npm run dev` or `yarn dev`

4. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

```bash
Open https://fe-test-catch.indragawe73.vercel.app/ with your browser to see the deployment result.
```

