import { Fragment } from 'react';
import Link from 'next/link'
import { useEffect, useState } from 'react'

const ProductCard = ( props ) => {
  
  const { listArray } = props;
  // console.log('props', props)

  function goDollar (val) {
    const goString = val.toString();
    const goLength = goString.length;
    const price = goLength - 2;
    const result = goString.substring(0, price);
    return result
  }

  function goCent (val) {
    const goString = val.toString();
    const goLength = goString.length;
    const price = goLength - 2;
    const result = goString.substring(price, goLength);
    return result
  }

  return (
        <Fragment>
          {listArray?
                
                <div className={"wrap_card " + (listArray.quantityAvailable == 0 ? 'opacity_sold' :null )}>
                  <div className="wrap_card_img">
                    <img className="img_product" src={listArray.imageUrl}  alt={listArray.name}/>
                    {listArray.quantityAvailable == 0 ?
                      <div className="sold_product">SOLD</div>
                      :null
                    }
                  </div>
                  
                  <div className="product_title">
                    {listArray.name}
                  </div>
                  
                  {listArray.retailPrice > 0 ?
                    <div className="retail_price"> Don't pay &#36; {goDollar(listArray.retailPrice)} </div>
                    :<div className="retail_price"></div>
                  }

                  <div className="wrap_sale_price">
                    <div className="dollarSign_sale_price">&#36;</div> 
                    <div className="dollar_sale_price">{goDollar(listArray.salePrice)}</div>
                    <div className="dollarSign_sale_price">{goCent(listArray.salePrice)}</div> 
                  </div>

                </div>

          :null}  
        </Fragment>

  )
}

export default ProductCard;
